# Imports
import sys 					# get command line arguments
import csv 					# csv IO
import urllib				# url fetching
import json					# json parsing
import subprocess			# external routine handling
import copy					# For dictionary copying

# call the fortran support lcc to geodetic routine
def lcpcgeo(point):
	# Call the routine
	str_geometry = subprocess.check_output(["./lcpgeo_cli", "1",
		str(point['projection']['phic']),
		str(point['projection']['xlonc']),
		str(point['projection']['truelat1']),
		str(point['projection']['truelat2']),
		str(point['geometry']['xloc']),
		str(point['geometry']['yloc']),
		'0.',
		'0.'])
	# split the string
	lst_geometry = str_geometry.split()
	# write the lat lon
	point['geometry']['lat'] = lst_geometry[0]
	point['geometry']['lon'] = lst_geometry[1]
	# output the the location dictionary
	return point

# API call
def get_data(point, radius, types, auth_key):
	# build the location string
	location = point['geometry']['lat']+','+point['geometry']['lon']
	# build the api url
	api_url = ('https://maps.googleapis.com/maps/api/place/nearbysearch/json'
		'?location=%s'
		'&radius=%s'
		'&types=%s'
		'&sensor=false&key=%s') % (location, radius, types, auth_key)
	# Get the results
	response = urllib.urlopen(api_url)
	json_raw = response.read()
	json_data = json.loads(json_raw)
	# print(json_data['status'])
	return json_data

# Regridding
def regrid(point, cell_size, types, auth_key, c_cell, depth):
	# Update the cell size
	cell_size = cell_size/2.
	depth = depth + 1
	space = '    '*depth
	print(space + 'Cell size is ' + str(cell_size))
	radius = 1000*cell_size*(2**.5)/2
	# Build the factor list
	factors = [ [-.5,-.5],
				[-.5, .5],
				[ .5,-.5],
				[ .5, .5]]
	# Do 4 subcells
	s_point = copy.deepcopy(point)
	for sub_cell in range(0,4):
		# calculate the new point
		s_point['geometry']['xloc'] = (float(point['geometry']['xloc'])+
										factors[sub_cell][0]*
										cell_size)
		s_point['geometry']['yloc'] = (float(point['geometry']['yloc'])+
										factors[sub_cell][1]*
										cell_size)

		# get the lat lon using the python function that calls the fortran function
		s_point = lcpcgeo(s_point)
		# print(s_point['geometry'])

		# call the api call function
		json_data = get_data(s_point, radius, types, auth_key)

		if json_data['status'] == 'OK':
			if 'next_page_token' in json_data:
				print(space + 'Too many results in cell, regriddinng')
				regrid(s_point, cell_size, types, auth_key, c_cell, depth)
			else:
				num_results = len(json_data['results'])
				print(space + 'Got '+str(num_results)+' results')
				write_out(json_data)

		elif json_data['status'] == 'ZERO_RESULTS':
			print(space + 'Zero restults')

		elif json_data['status'] == 'REQUEST_DENIED':
			print(space + 'API request denied')
			exit()

		elif json_data['status'] == 'OVER_QUERY_LIMIT':
			print(space + 'Exceded the query limit at cell ' + str(c_cell))
			exit()

# Write the csv
def write_out(json_data):
	for row in json_data['results']:
		res_place_id = row['place_id']
		res_name = row['name']
		res_lat = row['geometry']['location']['lat']
		res_lon = row['geometry']['location']['lng']
		res_types = row['types']
		# Write to the csv
		writer.writerow({
						'place_id': res_place_id,
						'name': res_name.encode('utf-8'),
						'lat': res_lat,
						'lon': res_lon,
						'types': res_types
						})

#	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
# Entry Point
#	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

# Get the arguments from the commad line
# Api key
auth_key = int(sys.argv[1])
# Input file
in_file_path = int(sys.argv[2])
# Start cell
s_cell = int(sys.argv[3])
# type
types = int(sys.argv[4])
# output file
out_file_path = int(sys.argv[5])

# Input
# Open the Latitude Longitude csv
# in_file = open('../4SWPA.csv')
# in_file = open('../4SWPA_1L.csv')
in_file = open(in_file_path)
reader = csv.DictReader(in_file)

# Output
# Open the output csv
out_file = open(out_file_path, 'w')
fieldnames = ['place_id','name','lat','lon','types']
writer = csv.DictWriter(out_file, fieldnames=fieldnames)
writer.writeheader()

# Define the cell size
cell_size = 4. # 4 km cells
print('Cell size is ' + str(cell_size))
radius = 1000*cell_size*(2**.5)/2
# # Auth key, this is not recommended practice
# auth_key = 'AIzaSyBSsIiQ-puqdpLz7An9d7sl3hZt5THzdx4'
# Place type for the API
# types = 'cafe'

# projection parameters
point = {
	'projection':{
		'phic': 40,
		'xlonc': -97,
		'truelat1': 45,
		'truelat2': 33
		},
	'geometry':{
		'xloc': 0,
		'yloc': 0,
		'lat': 0,
		'lon': 0
		}
	}

#  Print the latitude and longitude of the centroids
c_cell = 0
depth = 0
for row in reader:
	# skip the lines before c_line
	if c_cell >= s_cell:
		# Print the row number
		print('Working on cell '+str(c_cell))

		# get the LCC x, y from the csv
		point['geometry']['xloc'] = row['LCC_x']
		point['geometry']['yloc'] = row['LCC_y']

		# get the lat lon using the python function that calls the fortran function
		point = lcpcgeo(point)
		# print(point['geometry'])

		# call the api call function
		json_data = get_data(point, radius, types, auth_key)

		if json_data['status'] == 'OK':
			if 'next_page_token' in json_data:
				print('Too many results in cell, regriddinng')
				regrid(point, cell_size, types, auth_key, c_cell, depth)
			else:
				num_results = len(json_data['results'])
				print('Got '+str(num_results)+' results')
				write_out(json_data)

		elif json_data['status'] == 'ZERO_RESULTS':
			print('Zero restults')

		elif json_data['status'] == 'REQUEST_DENIED':
			print('API request denied')
			exit()

		elif json_data['status'] == 'OVER_QUERY_LIMIT':
			print('Exceded the query limit at cell ' + str(c_cell))
			exit()


	# update counter
	c_cell = c_cell + 1

	# # Make the url
	# location = row['Latitude']+","+row['Longitude']
	# # print(location)
	# api_url = ('https://maps.googleapis.com/maps/api/place/nearbysearch/json'
 #           '?location=%s'
 #           '&radius=%s'
 #           '&types=%s'
 #           '&sensor=false&key=%s') % (location, radius, types, auth_key)
	# # print(api_url)
	# # Get the results
	# response = urllib.urlopen(api_url)
	# json_raw = response.read()
	# json_data = json.loads(json_raw)
	# print(json_data.keys())
	# # print(json_data['status'])
	# # print(json_data['results'][0]['name'])
	# print('Working on cell '+str(i)+', API Request response status = '+json_data['status'])
	# i = i+1
	# if json_data['status'] == 'OK':
	# 	num_results = len(json_data['results'])
	# 	print('Got '+str(num_results)+' results')
	# 	for row in json_data['results']:
	# 		res_place_id = row['place_id']
	# 		res_name = row['name']
	# 		res_lat = row['geometry']['location']['lat']
	# 		res_lon = row['geometry']['location']['lng']
	# 		# Write to the csv
	# 		writer.writerow({
	# 						'place_id': res_place_id,
	# 						'name': res_name.encode('utf-8'),
	# 						'lat': res_lat,
	# 						'lon': res_lon
	# 						})
